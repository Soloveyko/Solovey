<?php
/**
 * | -----------------------------
 * | Created by expexes on 29.11.17/23:05.
 * | Site: teslex.tech
 * | ------------------------------
 * | Controller.php
 * | ---
 */

namespace Solovey\Controller;

class XController implements Controller
{
	/**
	 * @param $template
	 * @param array $vars
	 * @throws \Exception
	 */
	protected function render($template, array $vars = array())
	{
		$root = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR;

		render($root . $template, $vars);
	}
	
	function index()
	{
		// TODO: Implement index() method.
	}
}