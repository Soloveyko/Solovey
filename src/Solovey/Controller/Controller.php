<?php
/**
 * | -----------------------------
 * | Created by exp on 4/1/18/11:50 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Controller.php
 * | ---
 */

namespace Solovey\Controller;


interface Controller
{
	function index();
}