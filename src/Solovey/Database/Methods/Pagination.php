<?php
/**
 * | -----------------------------
 * | Created by exp on 4/14/18/3:21 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Pagination.php
 * | ---
 */

namespace Solovey\Database\Methods;


use Solovey\Database\Database;
use function a2o;
use function array_push;
use function ceil;

class Pagination
{
	private $className = null;
	private $table = null;
	private $keys = [];
	private $perPage = null;
	private $criteria = [];
	
	/**
	 * Pagination constructor.
	 * @param string $className
	 * @param int $perPage
	 * @param array $native
	 * @throws \Solovey\Exception\SoloveyException
	 */
	public function __construct($className, int $perPage = 10, array $native)
	{
		$this->className = $className;
		$this->perPage = $perPage;
		$this->criteria = $native;
		
		$this->table = Crud::normalizeByClass($this->className)['table'];
		$this->keys = Crud::normalizeByClass($this->className)['data']['keys'];
	}
	
	/**
	 * @return int
	 */
	function count()
	{
		$query = "SELECT Count(*) FROM $this->table";
		
		if (empty($this->criteria))
			$result = ceil(Database::query($query)->execute()->fetchColumn() / $this->perPage);
		else
			$result = ceil(Database::query($query . ' WHERE ' . $this->criteria[0] . ' ')->data($this->criteria[1])->execute()->fetchColumn() / $this->perPage);
		
		return $result;
	}
	
	/**
	 * @param int $page
	 * @param array $fields
	 * @return array
	 */
	function page(int $page, $fields = [])
	{
		$offset = ($page - 1) * $this->perPage;
		
		foreach ($fields as $index => $field)
			if (!in_array($field, $this->keys))
				unset($fields[$index]);
		
		$fields = empty($fields) ? '*' : implode(',', $fields);
		
		$query = "SELECT $fields FROM $this->table";
		$limits = "LIMIT $this->perPage OFFSET $offset";
		
		if (empty($this->criteria))
			$result = Database::query("$query $limits")->execute()->fetchAll();
		else
			$result = Database::query("$query WHERE {$this->criteria[0]} $limits")->data($this->criteria[1])->execute()->fetchAll();
		
		$all = [];
		
		foreach ($result as $res) {
			array_push($all, a2o($res, $this->className));
		}
		
		return $all;
	}
}