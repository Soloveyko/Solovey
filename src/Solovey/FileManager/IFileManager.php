<?php
/**
 * | -----------------------------
 * | Created by exp on 4/29/18 5:56 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | IFileManagerr.php
 * | ---
 */


namespace Solovey\Solovey\FileManager;


interface IFileManager
{
	
	function save($file);
	
	function delete($file);
	
	function move($file, $to);
	
	function rename($file, $new);
	
	function get($folder = '/');
	
}