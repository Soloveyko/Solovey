<?php
/**
 * | -----------------------------
 * | Created by expexes on 29.11.17/22:59.
 * | Site: teslex.tech
 * | ------------------------------
 * | bootstrap.php
 * | ---
 */

session_start();

use Solovey\Exception\SoloveyException;
use Solovey\Solovey;
use SoloveyRouter\Router;

try {
	
	// Have access??
	if (startsWith($_SERVER['REQUEST_URI'], '/app/') ||
		startsWith($_SERVER['REQUEST_URI'], '/engine/') ||
		startsWith($_SERVER['REQUEST_URI'], '/pages/') ||
		$_SERVER['REQUEST_URI'] === '/app' ||
		$_SERVER['REQUEST_URI'] === '/engine' ||
		$_SERVER['REQUEST_URI'] === '/pages'
	) {
		throw new SoloveyException('Access denied', 403);
	}
	
	// Load application
	require_once $_SERVER['DOCUMENT_ROOT'] . "/app/start.php";
	
	// Default root route
	Router::GET('/', function () {
		$version = Solovey::$version;
		print "
		<style>
		
		.solovey {
			color: #333;
			font-family: Monospaced,sans-serif;
			text-align: center;
		}
		
		</style>
		<h3 class='solovey'>Solovey $version</h3>
		";
		
		phpinfo();
	});
	
	// Load static content
	if (startsWith($_SERVER['REQUEST_URI'], '/static/')) {
		header("Content-type: " . getMimeType($_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']));
		readfile($_SERVER['SCRIPT_FILENAME']);
		
		return false;
	}
	
	Router::start($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
} catch (Exception $e) {
	$func = Solovey::$catchClojure;
	if (is_null($func)) {
		error($e->getMessage(), $e->getCode());
	} else {
		call_user_func_array($func, [$e]);
	}
}